<div id="popup" class="overlay">
    <div class="popup">
        <a class="close" href="#">&times;</a>
        <div class="content">
            {widget name="advanced_gmaps"}
        </div>
    </div>
</div>

<button id="localiser">
    <a href="#popup" rel="modal:open" style="color: #fff"> Géo-localiser nos magasins </a>
</button>

<style>
    .popup .close {
        position: absolute;
        top: -4px !important;
        right: 3px !important;
    }
    #localiser {
        background-color: #2fb5d2;
        padding: 5px 17px;
        border: none;
        color: #fff;
        position: relative;
        left: 15PX;
        top: 6px;
    }

    .overlay:target {
        z-index: 999;
    }

    .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }

    .popup {
        margin: 70px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 30%;
        position: relative;
        transition: all 5s ease-in-out;
    }

    .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
    }
    .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
    }
    .popup .close:hover {
        color: #06D85F;
    }
    .popup .content {
        max-height: 30%;
        overflow: auto;
    }

    @media screen and (max-width: 700px){

        .popup{
            width: 70%;
        }
    }
</style>