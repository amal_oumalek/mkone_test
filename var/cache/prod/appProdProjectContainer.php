<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerE4mhcxa\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerE4mhcxa/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerE4mhcxa.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerE4mhcxa\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerE4mhcxa\appProdProjectContainer([
    'container.build_hash' => 'E4mhcxa',
    'container.build_id' => '666d7866',
    'container.build_time' => 1598087730,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerE4mhcxa');
